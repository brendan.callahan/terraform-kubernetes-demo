data "digitalocean_kubernetes_versions" "example" {
  version_prefix = "1.20."
}

resource "digitalocean_kubernetes_cluster" "demo_control_plane" {
  name                = "demo-control-plane"
  region              = "nyc3"
  auto_upgrade = true
  surge_upgrade = true
  version             = data.digitalocean_kubernetes_versions.example.latest_version

  node_pool {
    auto_scale = true
    name           = "autoscale-worker-pool"
    size              = "s-2vcpu-4gb"
    min_nodes = 3
    max_nodes = 5
  }
}